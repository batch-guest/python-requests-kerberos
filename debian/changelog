python-requests-kerberos (0.11.0-3) UNRELEASED; urgency=medium

  * d/control: Use team+openstack@tracker.debian.org as maintainer

 -- Ondřej Nový <onovy@debian.org>  Fri, 03 Aug 2018 06:06:04 +0200

python-requests-kerberos (0.11.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Thomas Goirand ]
  * Add missing dh-python build-depends.

 -- Thomas Goirand <zigo@debian.org>  Mon, 02 Apr 2018 18:16:14 +0200

python-requests-kerberos (0.11.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).
  * d/rules: Changed UPSTREAM_GIT protocol to https

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Updating maintainer field.
  * Running wrap-and-sort -bast.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Updating standards version to 4.0.1.
  * Updating standards version to 4.1.0.

  [ Thomas Goirand ]
  * New upstream release (Closes: #880599).
  * Fixed (build-)depends for this release.
  * Using pkgos-dh_auto_install.
  * Removed all patches.

 -- Thomas Goirand <zigo@debian.org>  Thu, 02 Nov 2017 22:33:24 +0100

python-requests-kerberos (0.7.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 16 Oct 2015 10:45:31 +0000

python-requests-kerberos (0.7.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed CVE-2014-8650_Handle_mutual_authentication.patch applied upstream.
  * Some (build-)depends updates.

 -- Thomas Goirand <zigo@debian.org>  Wed, 02 Sep 2015 19:50:21 +0000

python-requests-kerberos (0.5-3) unstable; urgency=medium

  * Fixed swapped short package descriptions of python2.x and python3.x binary
    packages. Thanks to Beatrice Torracca <beatricet@libero.it> for the bug
    repport. (Closes: #782185)

 -- Thomas Goirand <zigo@debian.org>  Thu, 09 Apr 2015 09:59:49 +0200

python-requests-kerberos (0.5-2) unstable; urgency=high

  * CVE-2014-8650: failure to handle mutual authentication. Applied upstream
    patch: CVE-2014-8650_Handle_mutual_authentication.patch (Closes: #768408).
    Thanks to Salvatore Bonaccorso <carnil@debian.org> for reporting it.

 -- Thomas Goirand <zigo@debian.org>  Mon, 10 Nov 2014 21:22:51 +0800

python-requests-kerberos (0.5-1) unstable; urgency=medium

  * Initial release. (Closes: #752858)

 -- Thomas Goirand <zigo@debian.org>  Thu, 12 Jun 2014 09:09:45 +0800
